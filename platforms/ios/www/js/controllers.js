angular.module('starter.controllers', [])

    .controller('DashCtrl', function($scope,surveys) {

      $scope.surveys = surveys.all();
      $scope.remove = function(survey) {
        surveys.remove(survey);
      }
    })

    .controller('SurveyDetailCtrl', function($scope, $stateParams, surveys) {
      $scope.survey = surveys.get($stateParams.surveyId);
    })
