angular.module('starter.services', [])

    .factory('surveys', function() {
      // Might use a resource here that returns a JSON array

      // Some fake testing data
      var surveys = [{
        id: 0,
        name: 'Event Planning Surveys',
        description: 'Need a complete view into every facet of your event? Ever had a meeting go bananas?'+
        ' Whether you’re planning a webinar, gala, class, barbecue, or a wedding, you need to make sure' +
        ' that every detail is in place so that your attendees—and you—get the most out of the gathering.' +
        ' With online meeting and event planning surveys, you can manage contacts, send invites, collect payments,' +
        ' get real-time reports, and gather valuable event feedback.'
      }, {
        id: 1,
        name: 'Academic Surveys',
        description: 'Put your theories to the test. Sound survey data makes your research conclusions stronger '+
        'and more compelling, providing direct evidence to support or refute your claims.'
      },{
        id: 2,
        name: 'Market Research Surveys',
        description: 'Your customers know the secret to your success—all you have to do is ask! Find expert tips '+
        'and examples on the best ways to get feedback.'
      }, {
        id: 3,
        name: 'Education Surveys',
        description: 'Whether you need to understand the factors that shape faculty satisfaction, or '+
        'feedback from students on a new course offering, an online survey can reveal data' +
        ' that will improve programs, processes, and overall achievement. Parents, students, ' +
        'teachers, professors, and administrators can all benefit from professional education surveys.'
      }, {
        id: 4,
        name: 'Customer Satisfaction Surveys',
        description: 'Find out what customers think about your company and how it compares to your competitors.'
      }];

      return {
        all: function() {
          return surveys;
        },
        remove: function(survey) {
          surveys.splice(surveys.indexOf(survey), 1);
        },
        get: function(surveyId) {
          for (var i = 0; i < surveys.length; i++) {
            if (surveys[i].id === parseInt(surveyId)) {
              return surveys[i];
            }
          }
          return null;
        }
      };
    });
